/****************************************[ server.js ]********************************************/
/* Fichier Backend */
/*      Ce fichier est le fichier principal qui gère la partie Backend. C'est lui qui est lancé
*       au démarrage du serveur. Ses fonctions sont : Travailler avec l'objet brains qui contient
*       tous les brainstormings, communiquer à l'aide de socket.io avec tous les clients, faire
*       des sauvegardes de tous les brainstormings.
*/

//Require des modules externes -------------------------------------------------------
var express = require('express');
var hbs = require('express-handlebars');
var fs = require('fs');
var formidable = require('formidable');
//Require des classes
var Brain = require('./class/Brain.class.js');
var Message = require('./class/Message.class.js');
var Contributeur = require('./class/Contributeur.class.js');
//Require des fichiers externes
var backups = require('./backups.js')

//Configuration du serveur -----------------------------------------------------------

//Initialisation d'une app avec le module Expressjs
var app = express();
//Définit que le serveur web express.js écoute sur le port 3000
var server = app.listen(3000);
//Check au démarrage
console.log('Serveur running!');

//Require de socket.io
var io = require('socket.io').listen(server);

//Définit le layout pour les pages du site avec Express-handlebars
app.engine('hbs', hbs({extname: 'hbs', defaultLayout: 'layout', layoutsDir: __dirname + '/views/'}));
app.set('view engine', 'hbs');

//Fonctions au démarrage du serveur --------------------------------------------------

//Objet qui contient tous les brainstormings existants.
//La liste des brainstormings est dynamique, aucun "archivage" est fait par le serveur.
//Quand un brainstorming est supprimé, il l'est pour de bon
// /!\ Si on redémarre le serveur, tout disparait
var brains = {};

//Fonction qui s'exécute toute les minutes pour supprimer les vieux brainstormings
setInterval(function() {
    //foreach qui passe dans tous les brainstormings
    Object.keys(brains).forEach(function(i) {
        if(!brains[i].admin_connected) {
            brains[i].timer_delete++;
            console.log(brains[i].timer_delete);
            //Si le brainstorming est plus vieux de 45min, il est supprimé
            if(brains[i].timer_delete > 45) {
                console.log('brain ' + i + ' deleted due to inactivity');
                io.of('contributeurs').in(i).emit('brain_deleted');
                io.of('beamers').in(i).emit('brain_deleted');
                delete brains[i];
                console.log(brains);
            }
        }else{
            brains[i].timer_delete = 0;
        }

    });
}, 1000 * 60);

//Supprimer le contenu du répertoir /ressources/download au démarrage du serveur
fs.readdir(__dirname + '/ressources/download', function(err, files) {
    if(files != null) {
        files.forEach(function(file) {
            console.log(file);
            fs.unlink(__dirname + '/ressources/download/' + file, (err) => {
                if (err) throw err;
            });
        });
    }
});

//Supprimer le contenu du répertoir /ressources/upload au démarrage du serveur
fs.readdir(__dirname + '/ressources/upload', function(err, files) {
    if(files != null) {
        files.forEach(function(file) {
            fs.unlink(__dirname + '/ressources/upload/' + file, (err) => {
                if (err) throw err;
            });
        });
    }
});

//Fonction qui fait qu'une sauvegarde en fichier se fasse toutes les 2 minutes
setInterval(function() {backups.create_backup(brains, io);}, (1000 * 5));
backups.read_backup(brains, Brain, Message, Contributeur);

//Rooting d'URL ---------------------------------------------------------------------

//Permet d'avoir des URL custom
app.get('/', function(req, res) {                         //Page d'accueil
res.render('index.hbs');
});

app.get('/beamer:token', function(req, res) {            //Page du beamer
    res.render('beamer.hbs', {token: req.params.token, namespace: 'beamers'});
});

app.get('/join', function(req, res) {                     //Page de join d'un brainstorming
res.render('join.hbs');
});

app.get('/admin:id', function(req, res) {                //Page administrateur
    res.render('admin.hbs', {id: req.params.id, namespace: 'admins'});
});

app.get('/user:token', function(req, res) {              //Page de contributeur
    res.render('user.hbs', {token: req.params.token, namespace: 'contributeurs'});
});

//Si aucun des chemins précédents n'a été détecté, c'est soit
//l'appel d'un fichier .css, .js, ... soit que la page n'existe
//pas --> page 404
app.get('*', function(req, res){
    //check si le fichier qu'on essaie d'atteindre existe (=est présent dans le dossier "ressources")
    var path = __dirname + '/ressources' + req.url;
    if (fs.existsSync(path)) {
        res.sendFile(path);
    }else{
        res.render('404.hbs');                              //Le fichier n'existe pas --> page 404
    }
});

//Lorsque le serveur reçoit un fichier avec la méthode POST
//--> un administrateur veut uploader un fichier sur son brainstorming
app.post('*', function(req, res) {
    //Utilisation du module Formidable
    var form = new formidable.IncomingForm();
    var file_name;
    //Parser (traiter) le formulaire qui arrive
    form.parse(req);
    //À chaque nouveau fichier qui arrive
    form.on('fileBegin', function (name, file){
        file.path = __dirname + '/ressources/upload/' + file.name;
        file_name = file.name;
    });
    //Si tout s'est terminé correctement
    form.on('end', function() {
        //La réponse en json pourra être comprise dans la fonction ajax du côté client
        res.json({upload_success: true, name: file_name});
        res.end();
    });
    //Si il y a eu une erreur
    form.on('error', function() {
        //La réponse en json pourra être comprise dans la fonction ajax du côté client
        res.json({upload_success: false})
        res.end();
    });
});

//Fonctions provenant de la page users ------------------------------------------------------------------------------------------------------------------------------------
io.of('contributeurs').on('connection', function(socket) {
    Brain.timer_refresh(socket);
    var token_contributeur = socket.handshake.query.token;
    var id_contributeur = socket.handshake.query.id_connected_to_brain;

    //Est-ce que le brainstorming avec le token existe ?
    socket.on('user_join', function() {
        if(token_contributeur in brains) {
            var contributeur;
            socket.join(token_contributeur);
            //Est-ce que l'utilisateur s'est déjà connecté auparavant ?
            if(id_contributeur in brains[token_contributeur].contributeurs) {
                //Réactive le compte, renvoie à l'utilisateur qui se reconnecte les infos de son "compte"
                //ainsi que les messages qu'il a déjà envoyé
                if(brains[token_contributeur].contributeurs[id_contributeur].active) {
                    //L'utilisateur est déjà connecté sur une autre page
                    io.of('contributeurs').to(brains[token_contributeur].contributeurs[id_contributeur].socket_id).emit('user_change_page');
                }
                //L'utilisateur se reconnecte
                brains[token_contributeur].contributeurs[id_contributeur].active = true;
                //Met la nouvelle id de socket.io dans l'objet Contributeur
                brains[token_contributeur].contributeurs[id_contributeur].socket_id = socket.id;
                //contributeur = brains[token_contributeur].contributeurs[id_contributeur];
                //Créer un array, y mettre tous les messages de l'utilisateur et lui renvoyer
                var messages = [];
                for(var i in brains[token_contributeur].messages) {
                    if(brains[token_contributeur].messages[i].id_contributeur == id_contributeur) {
                        messages.push(brains[token_contributeur].messages[i]);
                    }
                }
                socket.emit('message_proposed', messages);
                socket.emit('user_accept', brains[token_contributeur].contributeurs[id_contributeur]);
                socket.emit('change_title', brains[token_contributeur].title);
            }else{
                contributeur = new Contributeur(socket.id, token_contributeur);
                brains[token_contributeur].contributeurs[contributeur.id] = contributeur;
                socket.emit('user_accept', contributeur);
                id_contributeur = contributeur.id;
            }
            //Modifier la liste des contributeurs connectés de la page admin
            io.of('admins').to(brains[token_contributeur].id_socket_admin).emit('edit_userlist', brains[token_contributeur].contributeurs);

        }else{
            socket.emit('user_refuse');
        }
    });

    socket.on('user_disconnect', function() {
        //Quand un utilisateur ferme son onglet --> met son compte en active = false
        //évite les crash au redémarrage du serveur
        if(token_contributeur in brains && id_contributeur in brains[token_contributeur].contributeurs) {
            brains[token_contributeur].contributeurs[id_contributeur].active = false;
            io.of('admins').to(brains[token_contributeur].id_socket_admin).emit('edit_userlist', brains[token_contributeur].contributeurs);
        }
    });

    //Quand un utilisateur souhaite proposer un message
    socket.on('propose_message', function(contenu) {
        var message_already_exists = false;
        for(var i in brains[token_contributeur].messages) {
            if(brains[token_contributeur].messages[i].contenu == contenu) {
                message_already_exists = true;
            }
        }
        if(message_already_exists) {
            socket.emit('message_already_exists');
        }else{
            //Si le message n'existe pas déjà
            //Création d'un nouveau message
            var message = new Message(contenu, id_contributeur);
            brains[token_contributeur].messages[message.id] = message;
            var messages = [];
            messages.push(message);
            socket.emit('message_proposed', messages);
            var array = [];
            array.push({message: message, user: brains[token_contributeur].contributeurs[id_contributeur]});
            //Envoie le message à l'administrateur
            io.of('admins').to(brains[token_contributeur].id_socket_admin).emit('new_messages', array);
            io.emit('test');
        }
    });
});

//Fonctions provenant de la page admin ------------------------------------------------------------------------------------------------------------------------------------
io.of('admins').on('connection', function(socket) {
    Brain.timer_refresh(socket);
    var token_admin;

    //Check à la connexion su l'ID que l'admin envoie avec sa connexion existe
    var found = false;
    for(var i in brains) {
        if(brains[i].id == socket.handshake.query.id) {
            //On place dans une variable le token du brainstorming (pour pouvoir
            //l'utiliser dans d'autres fonctions socket sans le redemander au client à chaque fois)
            token_admin = brains[i].token;
        }
    }
    if(!found) {
        //S'il n'existe pas c'est que le brainstorming n'existe pas
        socket.emit('admin_refuse');
    }

    socket.on('admin_join', function(id) {
        //Lorsque l'administrateur veut se connecter
        var brain;
        var found = false;
        for(var i in brains) {
            if(brains[i].id == id) {
                found = true;
                brain = brains[i];
            }
        }
        //Check à la connexion si l'ID que l'admin envoie avec sa connexion existe
        if(found) {
            socket.emit('admin_accept', brain.token, brain.title);
            //Rejoindre la "room" des admins
            socket.join('admins');
            //Déconnecte l'ancient admin
            io.of('admins').to(brains[brain.token].id_socket_admin).emit('admin_refuse');
            //Met à jour le le socket_id
            brains[brain.token].id_socket_admin = socket.id;
            console.log('admin socket id changed to ' + brains[brain.token].id_socket_admin + ' for brain ' + brains[brain.token].token);
            //Dis au serveur qu'il vient de se connecter (arrêter le countdown pour supprimer le brains à cause d'inactivité)
            brains[brain.token].admin_connected = true;
            //Donne la liste des messages sur le brainstorming
            var messages_in_board = [];
            for(var i in brain.messages) {
                if(brain.messages[i].on_board == true) {
                    messages_in_board.push(brain.messages[i]);
                }
            }
            socket.emit('add_to_board', messages_in_board);
            //Donne la liste des contributeurs
            socket.emit('edit_userlist', brain.contributeurs);
            //Donne la liste des messages présents dans la liste d'attente
            var array = [];
            for(var i in brain.messages) {
                if(brain.messages[i].on_board == false) {
                    if(brain.messages[i].id_contributeur != null) {
                        array.push({message: brain.messages[i], user: brain.contributeurs[brain.messages[i].id_contributeur]});
                    }else{
                        array.push({message: brain.messages[i], user: {color: '#ffffff'}});
                    }
                }
            }
            socket.emit('new_messages', array);
        }
    });

    socket.on('accept_proposition', function(id_msg) {
        //Lorsque l'administrateur choisis de déplacer un message depuis la liste des messages en attente
        //Jusque dans le brainstorming
        var messages = [];
        messages.push(brains[token_admin].messages[id_msg]);
        socket.emit('add_to_board', messages);
        io.of('beamers').in(token_admin).emit('add_to_board', messages);
        //On fait le "on_board" après ajouter le message sur le board pour que le client puisse
        //Détecter si c'est la première fois que le message est mis sur le board
        // --> Si c'est la première fois qu'on l'ajoute, il se placera au centre de la page
        //Si ça n'est pas la première fois les coordonées du messages sont celles qui sont enregistrées
        brains[token_admin].messages[id_msg].on_board = true;
    });

    //Lorsque l'administrateur refuse une proposition
    socket.on('refuse_proposition', function(id_msg) {
        //Faire un if() pour checker si l'utilisateur existe (le message pourrait être ajouté via un upload de brainstorming --> contributeur n'existe pas)
        if(brains[token_admin].messages[id_msg].id_contributeur != null) {
            //Envoyer à l'utilisateur qui a envoyé le message qu'il est refusé
            io.of('contributeurs').to(brains[token_admin].contributeurs[brains[token_admin].messages[id_msg].id_contributeur].socket_id).emit('message_deleted', id_msg);
        }
        //Suppression du message
        delete brains[token_admin].messages[id_msg];
    });

    //Lorsque l'administrateur change la taille ou l'orientation du message
    socket.on('scale_rotate_note', function(data) {
        if(data.id_msg in brains[token_admin].messages) {
            brains[token_admin].messages[data.id_msg].scale = data.scale;
            brains[token_admin].messages[data.id_msg].rotation = data.rotation;
        }
        io.of('beamers').in(token_admin).emit('note_scaled_rotated', data);
    });

    //Lorsque l'administrateur déplace un message
    socket.on('move_note', function(data) {
        io.of('beamers').in(token_admin).emit('note_moved', data);
        if(data.id_msg in brains[token_admin].messages) {
            brains[token_admin].messages[data.id_msg].x = data.x;
            brains[token_admin].messages[data.id_msg].y = data.y;
        }
    });

    //Lorsque l'administrateur se déplace sur le brainstorming
    socket.on('move_canvas', function(data) {
        io.of('beamers').in(token_admin).emit('canvas_moved', data);
    });

    //Lorsque l'administrateur zoom sur le brainstorming
    socket.on('zoom_canvas', function(data) {
        io.of('beamers').in(token_admin).emit('canvas_zoomed', data);
    });

    //Lorsque l'administrateur change le texte d'un message
    socket.on('change_text', function(data) {
        if(data.id_msg in brains[token_admin].messages) {
            brains[token_admin].messages[data.id_msg].contenu = data.text;
        }
        io.of('beamers').in(token_admin).emit('change_text', data);
    });

    //Lorsque l'administrateur change le titre du brainstorming
    socket.on('change_title', function(text) {
        io.of('beamers').in(token_admin).emit('change_title', text);
        io.of('contributeurs').in(token_admin).emit('change_title', text);
        brains[token_admin].title = text;
    })

    //Lorsque l'administrateur change la couleur d'un message
    socket.on('change_color', function(data) {
        io.of('beamers').in(token_admin).emit('change_color', data);
        brains[token_admin].messages[data.id_msg].color = data.color;
    });

    //Lorsque l'administrateur supprime un message du brainstorming
    socket.on('delete_note_from_board', function(id_msg) {
        io.of('beamers').in(token_admin).emit('delete_note', id_msg);
        if(brains[token_admin].messages[id_msg].id_contributeur in brains[token_admin].contributeurs) {
            io.of('contributeurs').in(token_admin).to(brains[token_admin].contributeurs[brains[token_admin].messages[id_msg].id_contributeur].socket_id).emit('message_deleted', id_msg);
        }
        delete brains[token_admin].messages[id_msg];
    });

    //Lorsque l'administrateur veut télécharger le brainstorming
    socket.on('brainstorming_download', function() {
        //Génère le contenu du fichier .csv
        var csv_file = brains[token_admin].gen_csv();
        var date = new Date();
        //Donne un nom au fichier (composé du token, de la date et de l'heure)
        var name = token_admin + "_" + date.getFullYear() + "_" + date.getMonth() + "_" + date.getDate();
        name += "_" + date.getHours() + "h" + date.getMinutes() + "m" + date.getSeconds() + "s.csv";
        var path = __dirname + "/ressources/download/" + name;
        //Création du fichier
        fs.writeFile(path, csv_file, function(err) {
            if(err) {
                socket.emit('download_error');
            }else{
                function send_back() {
                    //Renvoie à l'administrateur qu'il peut télécharger le fichier
                    socket.emit('download_ready', name);
                }
                //Donne un temps d'attente pour que le spinner soit visible pendant un minimum de temps sur le client
                setTimeout(send_back, 1000);
                //fonction qui s'occupe de supprimer le fichier à télécharger 60 secondes après l'avoir créé
                function delete_file() {
                    fs.unlink(path, (err) => {
                        if (err) throw err;
                    });
                }
                setTimeout(delete_file, 1000 * 60);
                //Une minute après avoir créé le fichier, il est supprimé du dossier des fichiers à télécharger
            }
        });
    });

    //Lorsque l'administrateur a fini d'uploader un fichier brainstorming, le serveur doit charger les données sur le brainstorming
    //et créer les nouveaux messages qui doivent s'ajouter sur le brainstorming
    socket.on('file_uploaded', function(name) {
        fs.readFile(__dirname + '/ressources/upload/' + name, 'utf-8', function(err, data) {
            if(err) {
                throw err;
            }else{
                brains[token_admin].load_brain(data, brains, io, socket, Message);
            }
            fs.unlink(__dirname + '/ressources/upload/' + name, (err) => {
                if (err) throw err;
            });
        });
    });

    //Lorsque l'administrateur se déconnecte, dit au serveur qu'il n'est plus là pour qu'il commence le countdown de 45min avant
    //que le brainstorming soit supprimé
    socket.on('admin_disconnect', function() {
        console.log('admin disconnected');
        if(token_admin in brains) {
            io.of('contributeurs').in(i).emit('admin_disconnected');
            brains[token_admin].admin_connected = false;
        }
    })
});

//Fonctions provenant de la page beamer -----------------------------------------------------------------------------------------------------------------------------------
io.of('beamers').on('connection', function(socket) {
    Brain.timer_refresh(socket);
    var token_beamer = socket.handshake.query.token;
    socket.on('beamer_join', function(token) {
        if(token in brains) {
            socket.emit('beamer_accept');
            //Rejoindre la "room" des beamers
            socket.join(token_beamer);
            //Donne la liste des messages sur le board
            var messages_in_board = [];
            for(var i in brains[token].messages) {
                if(brains[token].messages[i].on_board == true) {
                    messages_in_board.push(brains[token].messages[i]);
                }
            }
            socket.emit('add_to_board', messages_in_board);
            socket.emit('change_title', brains[token].title)
        }else{
            socket.emit('beamer_refuse');
        }
    });
});

io.on('connection', function(socket) {
    //Fonctions provenant de la page d'accueil --------------------------------------------------------------------------------------------------------------------------------
    socket.on('create_brain', function() {
        var brain = new Brain(brains);
        brains[brain.token] = brain;
        console.log(brains[brain.token]);
        socket.emit('brain_created', brains[brain.token]);
    });

    //Fonctions provenant de la page de join ----------------------------------------------------------------------------------------------------------------------------------
    socket.on('join_ask', function(token) {
        //Pour savoir si un brain existe ou non
        if(token in brains) {
            socket.emit('join_ask_true');
        }else{
            socket.emit('join_ask_false');
        }
    });
});
