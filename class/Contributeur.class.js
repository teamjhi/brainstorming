/****************************************[ Contributeur.class.js ]********************************************/
/* Fichier Backend */
/*      Ce fichier contient la classe Contributeurs qui va créer des objets utiles à la pages
*       /server.js.
*       Chaque contributeur correspond à un utilisateur qui rejoint un brainstorming excepté l'administrateur
*/

module.exports = class Contributeur {
    //Constructeur de la classe Contributeur
    constructor(socket_id, token_brain_connected, color, active, id) {
        //Chaque déclaration de variable peut soit avoir une valeur par défaut soit être définie à
        //l'appel du constructeur (p.ex. quand le serveur recréé les objets en chargeant le contenu
        //d'un fichier backup)
        this.socket_id = socket_id;
        this.token_brain_connected = token_brain_connected;
        this.color = color || this.gen_color();
        this.active = active || true;
        this.id = id || this.gen_id();
    }

    //Génère une couleur aléatoire qui sera attribuée au contributeur
    gen_color(){
        var r = Math.abs(Math.floor((((Math.random() * 254)) + 249) / 2));
        var g = Math.abs(Math.floor((((Math.random() * 254)) + 184) / 2));
        var b = Math.abs(Math.floor((((Math.random() * 254)) + 72) / 2));
        return '#' + r.toString(16) + g.toString(16) + b.toString(16);
    }

    //Génère un ID aléatoire qui sera attribuée au contributeur
    gen_id() {
        var text = "";
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 40; i++)
        text += chars.charAt(Math.floor(Math.random() * chars.length));
        return text;
    }
}
