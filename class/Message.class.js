/****************************************[ Message.class.js ]********************************************/
/* Fichier Backend */
/*      Ce fichier contient la classe Message qui va créer des objets utiles à la pages /server.js.
*/

module.exports = class Message {
    //Constructeur de la classe Message
    constructor(contenu, id_contributeur, id, on_board, x, y, scale, rotation, color) {
        //Chaque déclaration de variable peut soit avoir une valeur par défaut soit être définie à
        //l'appel du constructeur (p.ex. quand le serveur recréé les objets en chargeant le contenu
        //d'un fichier backup)
        this.contenu = contenu;
        this.id_contributeur = id_contributeur || null;
        this.id = id || this.gen_id();
        this.on_board = on_board || false;
        this.x = x || 0;
        this.y = y || 0;
        this.scale = scale || 1;
        this.rotation = rotation || 0;
        this.color = color || "black";
    }

    //Génère un ID aléatoire qui sera attribuée au message
    gen_id() {
        var text = "";
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 100; i++)
        text += chars.charAt(Math.floor(Math.random() * chars.length));
        return text;
    }
}
