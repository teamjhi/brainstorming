/****************************************[ Brain.class.js ]********************************************/
/* Fichier Backend */
/*      Ce fichier contient la classe Brain qui va créer des objets utiles à la pages /server.js.
*       Chaque brain contient une liste de ses messages, de ses contributeurs et les informations
*       de son administrateur
*       Il contient également quelques foncttions utiles, comme load_brain() qui va remettre dans
*       l'objet brains les informations provenant d'un fichier de sauvegarde
*/

module.exports = class Brain {
    //Constructeur de la classe Brain
    constructor(brains, id_socket_admin, token, id, title) {
        //Chaque déclaration de variable peut soit avoir une valeur par défaut soit être définie à
        //l'appel du constructeur (p.ex. quand le serveur recréé les objets en chargeant le contenu
        //d'un fichier backup)
        this.id_socket_admin = "" || id_socket_admin;
        this.token = token || this.gen_token(brains);
        this.id = id || this.gen_id();
        this.title = title || "Sujet du brainstorming";
        this.messages = {};
        this.contributeurs = {};
        this.timer_delete = 0;
        this.admin_connected = false;
    }

    //Génère un token de 6 chiffres aléatoire
    //Est sûr que le token n'est pas déjà utilisé en utilisant le contenu de l'objet brains
    gen_token(brains) {
        var token_already_taken;
        var random_token;
        do{
            random_token = Math.floor((Math.random() * 900000) + 100000);
            //check si le token existe déjà
            if(random_token in brains) {
                random_token = true;
            }else{
                return random_token;
            }
        }while (token_already_taken);
    }

    //Retourne une chaine de caractères aléatoire de 200 caractères de long
    gen_id() {
        var text = "";
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 200; i++)
        text += chars.charAt(Math.floor(Math.random() * chars.length));
        return text;
    }

    //Retourne dans un format fait pour être enregistré dans un fichier csv le contenu du brainstorming actuel
    //uniquement le titre du brainstorming et ses messages
    gen_csv() {
        var csv = "Titre :;" + this.title + "\n\n";
        csv += "Messages:\nContenu;present sur le tableau;position X;position Y;Taille;Rotation;Couleur\n";
        //Pour chaque message
        for(var i in this.messages) {
            csv += this.messages[i].contenu + ";" + this.messages[i].on_board + ";" + this.messages[i].x + ";";
            csv += this.messages[i].y + ";" + this.messages[i].scale + ";" + this.messages[i].rotation + ";" + this.messages[i].color + "\n";
        }
        csv += "";
        return csv;
    }

    //S'occupe de mettre dans le brainstorming actuel les données provenant d'un fichier CSV qui a été uploadé
    load_brain(data, brains, io, socket, Message) {
        //Place le contenu du fichier uploadé dans un tableau à 2 dimenstions
        var csv = [];
        var rows = data.split('\n');
        for(var i = 0; i < rows.length; i++) {
            var lines = rows[i].split(';');
            csv[i] = [];
            for(var j = 0; j < lines.length; j++) {
                csv[i][j] = lines[j];
            }
        };
        brains[this.token].title = csv[0][1];
        //Envoie un message à tout le monde connecté sur le brainstorming que le titre a changé
        io.of('beamers').emit('change_title', csv[0][1]);
        io.of('contributeurs').emit('change_title', csv[0][1]);
        socket.emit('change_title', csv[0][1])
        //On parcours la liste des messages dans le fichier uploadé
        //On commence à 4 car les messages se trouvent à la ligne 4 dans le fichier csv
        //On fait -1 car on enlève la dernière ligne (vide)
        var messages = {};
        for(var i = 4; i < csv.length - 1; i++) {
            //Pour chaque nouveau message uploadé on créé un objet Message
            var message = new Message(csv[i][0]);
            if(csv[i][1] == 'true') {
                message.on_board = true;
            }else{
                message.on_board = false;
            }
            //Mettre les données dans l'objet message créé
            message.x = csv[i][2];
            message.y = csv[i][3];
            message.scale = csv[i][4];
            message.rotation = csv[i][5];
            message.color = csv[i][6];
            messages[message.id] = message;
        }
        brains[this.token].messages = messages;

        //Messages se trouvant dans la liste en attente
        var array = [];
        for(var i in messages) {
            if(messages[i].on_board == false) {
                array.push({message: messages[i], user: {color: '#ffffff'}});
            }
        }
        //Envoi de la liste à l'administrateur
        socket.emit('new_messages', array);

        //Messages se trouvant sur le brainstorming
        var messages_in_board = [];
        for(var i in messages) {
            if(messages[i].on_board == true) {
                messages_in_board.push(messages[i]);
            }
        }
        //Envoi de la liste à l'administrateur et aux beamers
        socket.emit('add_to_board', messages_in_board);
        io.in('beamers').emit('add_to_board', messages_in_board);
    }

    //Fonction qui s'occupe de checker si chaque utilisateur qui se connecte
    //a actualisé sa page avant de se connecter. Si ça n'est pas le cas,
    //un message lui est envoyé pour qu'il refresh sa page
    // --> utile pour refresh les clients après un redémarrage du serveur
    static timer_refresh(socket) {
        //Si le client envoie un message 'page-refreshed', la boucle est stoppée
        socket.on('page-refreshed', function() {
            clearInterval(timer_logout);
        });

        //Démarrage du timer
        var timer = 0;
        //Boucle qui l'incrémente
        var timer_logout = setInterval(function () {
            timer++;
            //Si le timer attends plus d'une seconde, le client doit refresh
            if(timer > 1) {
                socket.emit('server_has_restarted');
            }

        }, 1000);
    }
}
