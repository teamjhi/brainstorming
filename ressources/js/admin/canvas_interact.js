/****************************************[ canvas_interact.js ]********************************************/
/* Fichier Backend */
/*      Ce fichier est utile à la page Administrateur
*       /!\ Attention, il existe le même fichier présent dans le répertoire /ressources/js/beamer/canvas_interact.js
*       mais l'autre fichier est fait pour la page beamer
*       Ce fichier permet d'interagir avec le brainstorming en général
*       Il permet de se déplacer sur le brainstorming et d'ajouter des messages sur le canvas
*/

$(function() {

    //Zoom sur le brainstorming ----------------------------------------------------------------------------------------
    zoom = $(window).width() / 1000;
    $('#brainstorming-canvas').css('zoom', zoom);

    $('#brainstorming-conteneur').bind('mousewheel', function(e){
        if(e.originalEvent.wheelDelta /120 > 0) {
            if(zoom < 4) {
                zoom += 0.1;
            }
        }else{
            if(zoom > 0.3) {
                zoom -= 0.1;
            }
        }
        $('#brainstorming-canvas').css('zoom', zoom);
        socket.emit('zoom_canvas', zoom);
    });

    //Déplacer le brainstorming ----------------------------------------------------------------------------------------

    var pos_x_canvas, pos_y_canvas, pos_x_mouse, pos_y_mouse;
    var grab_brain = false;
    var offset = $("#brainstorming-conteneur").offset();
    var can_drag = true;

    //ORDINATEUR ----------------------------------------------------------------------
    //Ne pas activer le drag si la souris passe sur un élement .no-canvas-drag
    $(document).on('mouseover', '.no-canvas-drag', function() {
        can_drag = false;
    });
    $(document).on('mouseout', '.no-canvas-drag', function() {
        can_drag = true;
    });
    //Quand l'utilisateur commence de cliquer
    $('#brainstorming-conteneur').mousedown(function(e) {
        if(can_drag) {
            grab_brain = true;
            pos_x_canvas = parseInt($('#brainstorming-canvas').css('margin-left'));
            pos_y_canvas = parseInt($('#brainstorming-canvas').css('margin-top'));
            pos_x_mouse = (e.pageX - offset.left) - $("#brainstorming-conteneur").width() / 2;
            pos_y_mouse = (e.pageY - offset.top) - $("#brainstorming-conteneur").height() / 2;
            $('#brainstorming-canvas').css('cursor', '-webkit-grabbing');
        }
    });
    //Quand l'utilisateur déplace la souris
    $('#brainstorming-conteneur').mousemove(function(e) {
        if(grab_brain) {
            var margin_left = (pos_x_canvas + 1/zoom * (-(pos_x_mouse - (e.pageX - offset.left) + $("#brainstorming-conteneur").width() / 2)));
            var margin_right = (pos_y_canvas + 1/zoom * (-(pos_y_mouse - (e.pageY - offset.top) + $("#brainstorming-conteneur").height() / 2)));
            $('#brainstorming-canvas').css('margin-left', Math.max(Math.min(margin_left, parseInt($('#brainstorming-canvas').css('width'))/2), -parseInt($('#brainstorming-canvas').css('width'))/2));
            $('#brainstorming-canvas').css('margin-top', Math.max(Math.min(margin_right, parseInt($('#brainstorming-canvas').css('height'))/2), -parseInt($('#brainstorming-canvas').css('height'))/2));
        }
    });
    //Quand l'utilisateur relâche la souris
    $('#brainstorming-conteneur').mouseup(function() {
        grab_brain = false;
        //Envoie les changements au serveur
        var obj = {margin_top : $('#brainstorming-canvas').css('margin-top'), margin_left : $('#brainstorming-canvas').css('margin-left')};
        socket.emit('move_canvas', obj);
        $('#brainstorming-canvas').css('cursor', '-webkit-grab');
    });

    //MOBILE----------------------------------------------------------------------
    //Quand l'utilisateur commence d'appuyer
    var brainstorming_canvas = document.getElementById('brainstorming-canvas');
    brainstorming_canvas.addEventListener('touchstart', function(e){
        pos_x_canvas = parseInt($('#brainstorming-canvas').css('margin-left'));
        pos_y_canvas = parseInt($('#brainstorming-canvas').css('margin-top'));
        pos_x_mouse = (e.changedTouches[0].pageX - offset.left) - $("#brainstorming-conteneur").width() / 2;
        pos_y_mouse = (e.changedTouches[0].pageY - offset.top) - $("#brainstorming-conteneur").height() / 2;
    }, false);
    //Quand l'utilisateur déplace son doigt
    brainstorming_canvas.addEventListener('touchmove', function(e){
        $('#brainstorming-canvas').css('margin-left', (pos_x_canvas + 1/zoom * (-(pos_x_mouse - (e.changedTouches[0].pageX - offset.left) + $("#brainstorming-conteneur").width() / 2))));
        $('#brainstorming-canvas').css('margin-top', (pos_y_canvas + 1/zoom * (-(pos_y_mouse - (e.changedTouches[0].pageY - offset.top) + $("#brainstorming-conteneur").height() / 2))));
    }, false);
});

// Déplacer un message de la liste d'attente jusque sur le brainstorming -----------------------------------------------------------------------------------
socket.on('add_to_board', function(data) {
    //Pour chaque message à ajouter sur le brainstorming
    for(i in data) {
        var html = '<div class="draggable-message no-canvas-drag message-on-board selectable-message" style="';
        if(data[i].on_board) {
            //Le message a déjà été mis auparavant sur le plateau, sa position est connue
            html += 'margin-left: ' + data[i].x + '; margin-top: '+ data[i].y + ';';
        }else{
            //C'est la première fois que le message est ajouté sur le plateau --> on le met au centre du brainstorming
            html += 'margin-left: 0px; margin-top: 0px; ';
        }
        html += 'transform: scale(' + data[i].scale + ') rotate(' + data[i].rotation + 'deg)"';
        html += 'id_msg="' + data[i].id + '">';
        html += '<div style="padding: 0px 30px"><textarea rows="1" data-autoresize class="vertical-center message-textarea textarea-autoresize no-selectable-message" disabled ';
        html += 'style="color: ' + data[i].color  + ';">' + data[i].contenu + '</textarea></div></div>';
        $('#canvas-messages').append(html);
        //Calculer la taille que l'élément textarea doit prendre pour que le texte entier s'affiche
        calculate_textarea_size();
    }
});
