/****************************************[ propositions_list.js ]********************************************/
/* Fichier Backend */
/*      Ce fichier est utile à la page Administrateur
*       Ce fichier s'occupe de tout ce qui est relatif au menu de la page administrateur
*/

//Ajout des messages dans le menu -------------------------------------------
socket.on('new_messages', function(data) {
    //Pour chaque nouveau message
    for(i in data) {
        var html = '';
        html += '<div class="message-list" style="display: none;">';
        html += '<label class="message-off-board" id_msg="' + data[i].message.id + '" on_board="' + data[i].message.on_board + '" ';
        html += 'style="box-shadow: -5px 0px 0px ' + data[i].user.color + ';" ';
        html += 'for="message-off-board-dropdown-checkbox-' + data[i].message.id + '"';
        html += '>';
        html += '<div class="message-off-board-contenu">';
        html += data[i].message.contenu;
        html += '</div>';
        html += '<input type="checkbox" class="message-off-board-dropdown-checkbox" id="message-off-board-dropdown-checkbox-' + data[i].message.id + '">';
        //Petit menu pour ajouter ou supprimer le message
        html += '<div class="message-off-board-dropbown">';
        html += '<div class="message-off-board-dropdown-element add_message_to_board" id_msg="' + data[i].message.id + '">Ajouter</div>';
        html += '<div class="message-off-board-dropdown-element admin_delete_message" id_msg="' + data[i].message.id + '">Supprimer</div>';
        html += '</div>';
        html += '</label>';
        html += '</div>';
        html += '</div>';
        $('#flex-panel-messages-list').prepend(html);
        $('#flex-panel-messages-list').children().first().slideDown('slow');
    }
});


//Modifier la liste des utilisateurs présents dans le menu -------------------------------------------
socket.on('edit_userlist', function(data) {
    var html = '';
    var count = 0;
    for(var i in data) {
        html += '<div class="button" style="';
        html += 'border-bottom: 5px solid ' + shadeColor(data[i].color, -15) + ';';
        html += 'border-top: 0px solid ' + shadeColor(data[i].color, -15) + '; position: relative; background-color: ';
        if(data[i].active) {
            html += data[i].color + '"><div class="vertical-center" style="color: white"><i class="fas fa-bolt"></i></div>';
        }else{
            html += shadeColor(data[i].color, 60) + '"><div class="vertical-center" style="color: #636363"><i class="fas fa-times"></i></div>';
        }
        html += '</div>';
        count++;
    }
    $('#flex-panel-contributeurs-list').html(html);
    $('#nbr-connected-contributeurs').html(count);
});

$(function() {
    //Fonctionnement des onglets du menu de la page
    $('[name=aside-panel-radio]').change(function() {
        if($('#messages-panel-radio').is(':checked')) {
            $('#flex-panel-conteneur').css('margin-left', '0%');
        }else if($('#contributeurs-panel-radio').is(':checked')) {
            $('#flex-panel-conteneur').css('margin-left', '-100%');
        }else if($('#settings-panel-radio').is(':checked')) {
            $('#flex-panel-conteneur').css('margin-left', '-200%');
        }
    });

    //Ouvreture du menu dropdown d'un message
    $(document).on('click', '.message-off-board-dropdown-checkbox', function() {
        $(this).next().toggleClass('open');
    });

    //Refus d'un message
    $(document).on('click', '.admin_delete_message', function() {
        socket.emit('refuse_proposition', $(this).attr('id_msg'));
        $(this).parent().parent().slideUp();
    });

    //Acceptation d'un message
    $(document).on('click', '.add_message_to_board', function() {
        socket.emit('accept_proposition', $(this).attr('id_msg'));
        $(this).parent().parent().slideUp();
    });
});
