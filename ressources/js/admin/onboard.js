/****************************************[ onboard.js ]********************************************/
/* Fichier Backend */
/*      Ce fichier est utile à la page Administrateur
*       Ce fichier s'occupe de tout ce qui est relatif à l'interaction avec les messages
*/

$(function() {
    //Sélection d'un message ----------------------------------------------------------------------------------------
    $(document).on('click', '.selectable-message', function() {
        //Ajoute au message les éléments qui permettent de l'éditer (couleur, taille, rotation, suppression)
        if(!('selected-message' in $(this)[0].classList)) {
            //Désélectionne tous les autres messages en premier
            $('.selectable-message').removeClass('selected-message');
            $('.message-textarea').prop('disabled', true);
            $('.edit-selected-message').remove();
            $(this).addClass('selected-message');
            $(this).children().first().children().first().prop('disabled', false);
            var html = '<div id="resize-message" class="no-message-drag edit-selected-message" style="display: none;"><i class="fas fa-arrows-alt-v"></i></div>';
            html += '<div id="rotate-message" class="no-message-drag edit-selected-message" style="display: none;"><i class="fas fa-sync-alt"></i></div>';
            html += '<div id="delete-message" class="no-message-drag edit-selected-message" style="display: none;"><i class="fas fa-trash"></i></div>';
            html += '<div id="menu-selected-message" class="no-message-drag edit-selected-message" style="display: none;">';
            html += '<div class="menu-selected-message-item selected-message-color-picker" style="background-color: black;"></div>';
            html += '<div class="menu-selected-message-item selected-message-color-picker" style="background-color: #e67e22;"></div>';//orange
            html += '<div class="menu-selected-message-item selected-message-color-picker" style="background-color: #e74c3c;"></div>';//rouge
            html += '<div class="menu-selected-message-item selected-message-color-picker" style="background-color: #8e44ad;"></div>';//violet
            html += '<div class="menu-selected-message-item selected-message-color-picker" style="background-color: #2980b9;"></div>';//bleu
            html += '<div class="menu-selected-message-item selected-message-color-picker" style="background-color: #27ae60;"></div>';//vert
            html += '</div>';
            $(this).append(html);
            $('.edit-selected-message').fadeIn('normal');
            //Mettre le message en dernier de la liste des messages (au dessus de tous les autres visuellement)
            $('#canvas-messages').append($(this));
        }
    });
    $('#brainstorming-canvas').click(function() {
        //Désélectionne tous les messages quand on clique sur l'élément brainstorming-canvas ou un de ses enfants...
        $('.selectable-message').removeClass('selected-message');
        $('.message-textarea').prop('disabled', true);
        $('.edit-selected-message').remove();
    }).on('click', '.selected-message', function() {
        //...sauf quand on clique sur le message sélectionné
        return false;
    });

    //Déplacement d'un message  ------------------------------------------------------------------------
    var pos_x_message_drag, pos_y_message_drag, pos_x_mouse_drag, pos_y_mouse_drag;
    var grab_message_drag = false;
    var offset = $("#brainstorming-conteneur").offset();
    //Quand l'utilisateur commence de cliquer
    $(document).on('mousedown', '.draggable-message.selected-message', function(e) {
        grab_message_drag = true;
        //Donne la distance x et y entre le message et le centre du canvas
        pos_x_message_drag = parseInt($(this).css('margin-left'));
        pos_y_message_drag = parseInt($(this).css('margin-top'));
        //Donne la distance x et y entre la souris et le centre du canvas
        pos_x_mouse_drag = (e.pageX - offset.left) - $("#brainstorming-conteneur").width() / 2;
        pos_y_mouse_drag = (e.pageY - offset.top) - $("#brainstorming-conteneur").height() / 2;
    }).on('mousedown', '.no-message-drag', function() {
        //Ne pas activer la fonction si l'utilisateur clique sur un élément ayant la classe 'no-message-drag'
        return false;
    });
    //Quand l'utilisateur déplace la souris
    $(document).on('mousemove', '#brainstorming-conteneur', function(e) {
        if(grab_message_drag) {
            $('.draggable-message.selected-message').css('margin-left', (pos_x_message_drag + 1/zoom * (-(pos_x_mouse_drag - (e.pageX - offset.left) + $("#brainstorming-conteneur").width() / 2))));
            $('.draggable-message.selected-message').css('margin-top', (pos_y_message_drag + 1/zoom * (-(pos_y_mouse_drag - (e.pageY - offset.top) + $("#brainstorming-conteneur").height() / 2))));
        }
    });
    //Quand l'utilisateur relâche la souris
    $(document).on('mouseup', '#brainstorming-conteneur', function() {
        if(grab_message_drag) {
            //Envoie les changements au serveur
            var obj = {}
            obj.x = $('.draggable-message.selected-message').css('margin-left');
            obj.y = $('.draggable-message.selected-message').css('margin-top');
            obj.id_msg = $('.draggable-message.selected-message').attr('id_msg');
            console.log(obj);
            socket.emit('move_note', obj);
            grab_message_drag = false;
        }
    });

    //Redimentionnement d'un message ------------------------------------------------------------------------

    //ordinauteur
    var grab_message_resize = false;
    //Quand l'utilisateur commence de cliquer
    $(document).on('mousedown', '#resize-message', function() {
        grab_message_resize = true;
    });
    //Quand l'utilisateur déplace la souris
    $(document).on('mousemove', '#brainstorming-canvas', function(e) {
        if(grab_message_resize) {
            var selected_message = $('#resize-message').parent();
            //Donne la distance en x et en y entre le centre du message et centre du canvas
            var pos_center_x_message = parseInt(selected_message.css('margin-left')) + selected_message.width()/2;
            var pos_center_y_message = parseInt(selected_message.css('margin-top')) + selected_message.height()/2;
            var current_rotation = getRotationDegrees($('.draggable-message.selected-message'));
            //Donne la distance en x et en y entre la souris et le centre du canvas
            var mouse_position_center_x = (-(parseInt($('#brainstorming-canvas').css('margin-left'))) + ((e.pageX - $("#brainstorming-conteneur").offset().left) / zoom)) - ($('#brainstorming-conteneur').width()/2) / zoom;
            var mouse_position_center_y = (-(parseInt($('#brainstorming-canvas').css('margin-top'))) + ((e.pageY) / zoom)) - ($('#brainstorming-conteneur').height()/2) / zoom;
            //Donne la distance de la souris par rapport au centre du message (Pythagore)
            var distance_mouse_message = Math.sqrt(Math.pow(Math.abs(pos_center_x_message - mouse_position_center_x), 2) + Math.pow(Math.abs(pos_center_y_message - mouse_position_center_y), 2));
            //donne la distance entre le centre du message et son bord inférieur droit (Pythagore)
            //Cette valeur ne change pas selon la propriété scale du message, elle ne change qu'au dépend du texte inscrit dans le textarea
            var distance_center_message_bottom_right = Math.sqrt(Math.pow(parseInt(selected_message.width()/2), 2) + Math.pow(parseInt(selected_message.height()/2), 2), 2);
            $('.draggable-message.selected-message').css('transform', 'rotate(' + current_rotation + 'deg) scale(' + (distance_mouse_message / distance_center_message_bottom_right) + ')');
        }
    });
    //Quand l'utilisateur relâche la souris
    $(document).on('mouseup', '#brainstorming-conteneur', function() {
        if(grab_message_resize) {
            var obj = {};
            obj.rotation = getRotationDegrees($('.draggable-message.selected-message'));
            obj.scale = getScaleValue($('.draggable-message.selected-message'));
            obj.id_msg = $('.draggable-message.selected-message').attr('id_msg');
            socket.emit('scale_rotate_note', obj);
            grab_message_resize = false;
        }
    });

    //Rotation d'un message -------------------------------------------------------------------------

    //ordinauteur
    //Quand l'utilisateur commence de cliquer
    var grab_message_rotate = false;
    $(document).on('mousedown', '#rotate-message', function(e) {
        grab_message_rotate = true;
    });
    //Quand l'utilisateur déplace la souris
    $(document).on('mousemove', '#brainstorming-canvas', function(e) {
        if(grab_message_rotate) {
            var selected_message = $('#rotate-message').parent();
            //Donne la position x et y entre le centre du message et le centre du canvas
            var pos_center_x_message_rotate = parseInt(selected_message.css('margin-left')) + selected_message.width()/2;
            var pos_center_y_message_rotate = parseInt(selected_message.css('margin-top')) + selected_message.height()/2;
            var current_scale = getScaleValue($('.draggable-message.selected-message'));
            //Donne la position x et y entre la souris et le centre du canvas
            var mouse_position_center_x = (-(parseInt($('#brainstorming-canvas').css('margin-left'))) + ((e.pageX - $("#brainstorming-conteneur").offset().left) / zoom)) - ($('#brainstorming-conteneur').width()/2) / zoom;
            var mouse_position_center_y = (-(parseInt($('#brainstorming-canvas').css('margin-top'))) + ((e.pageY) / zoom)) - ($('#brainstorming-conteneur').height()/2) / zoom;
            var degree_rotation = -(Math.atan2(mouse_position_center_x - pos_center_x_message_rotate, mouse_position_center_y - pos_center_y_message_rotate) * 180 / Math.PI) - 90;
            $('.draggable-message.selected-message').css('transform', 'rotate(' + degree_rotation + 'deg) scale(' + current_scale + ')');
        }
    });
    //Quand l'utilisateur relâche la souris
    $(document).on('mouseup', '#brainstorming-conteneur', function() {
        if(grab_message_rotate) {
            console.log($('.draggable-message.selected-message').css('transform'));
            var obj = {};
            obj.rotation = getRotationDegrees($('.draggable-message.selected-message'));
            obj.scale = getScaleValue($('.draggable-message.selected-message'));
            obj.id_msg = $('.draggable-message.selected-message').attr('id_msg');
            socket.emit('scale_rotate_note', obj);
            grab_message_rotate = false;
        }
    });

    //Changement du text d'un message
    $(document).on('change keyup keydown paste', '.message-textarea', function() {
        var obj = {};
        obj.text = $(this).val();
        obj.id_msg = $('.draggable-message.selected-message').attr('id_msg');
        socket.emit('change_text', obj);
        $(this).attr('cols', Math.min($(this).val().length + 2, 30));
        calculate_textarea_size();
    });

    //Changement de la couleur d'un message
    $(document).on('mouseup', '.selected-message-color-picker', function() {
        console.log('color clicked');
        var color = $(this).css('background-color');
        $('.selected-message').children().first().children().first().css('color', color);
        socket.emit('change_color', {id_msg: $('.draggable-message.selected-message').attr('id_msg'), color: color});
    });

    //Suppression d'un message
    $(document).on('mouseup', '#delete-message', function() {
        var message_to_delete = $(this).parent();
        socket.emit('delete_note_from_board', $('.selected-message').attr('id_msg'));
        //Déselection du message sélectionné
        $('.selectable-message').removeClass('selected-message');
        $('.message-textarea').prop('disabled', true);
        $('.edit-selected-message').remove();
        //Disparition
        message_to_delete.fadeOut('slow');
    });

});
