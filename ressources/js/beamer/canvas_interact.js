/****************************************[ canvas_interact.js ]********************************************/
/* Fichier Backend */
/*      Ce fichier est utile à la page Beamer
*       /!\ Attention, il existe le même fichier présent dans le répertoire /ressources/js/admin/canvas_interact.js
mais l'autre fichier est fait pour la page administrateurs
*       Ce fichier permet d'interagir avec le brainstorming en général
*       Il permet de se déplacer sur le brainstorming et d'ajouter des messages sur le canvas
*/

$(function() {

    //Zoom sur le brainstorming ----------------------------------------------------------------------------------------
    zoom = $(window).width() / 1000;
    $('#brainstorming-canvas').animate({ 'zoom': zoom }, 200);

    $('#brainstorming-conteneur').bind('mousewheel', function(e){
        if(e.originalEvent.wheelDelta /120 > 0) {
            if(zoom < 4) {
                zoom += 0.1;
            }
            $('#brainstorming-canvas').css('zoom', zoom);
        }
        else{
            if(zoom > 0.3) {
                zoom -= 0.1;
            }
            $('#brainstorming-canvas').css('zoom', zoom);
        }
        console.log(zoom);
    });

    //Déplacer le brainstorming ----------------------------------------------------------------------------------------

    var pos_x_canvas, pos_y_canvas, pos_x_mouse, pos_y_mouse;
    var grab_brain = false;
    var offset = $("#brainstorming-conteneur").offset();
    //ORDINATEUR --------------------------------------------------------------
    //Quand l'utilisateur commence de cliquer
    $('#brainstorming-canvas').mousedown(function(e) {
        grab_brain = true;
        pos_x_canvas = parseInt($('#brainstorming-canvas').css('margin-left'));
        pos_y_canvas = parseInt($('#brainstorming-canvas').css('margin-top'));
        pos_x_mouse = (e.pageX - offset.left) - $("#brainstorming-conteneur").width() / 2;
        pos_y_mouse = (e.pageY - offset.top) - $("#brainstorming-conteneur").height() / 2;
        $('#brainstorming-canvas').css('cursor', '-webkit-grabbing');
    });
    //Quand l'utilisateur déplace la souris
    $('#brainstorming-canvas').mousemove(function(e) {
        if(grab_brain) {
            $('#brainstorming-canvas').css('margin-left', (pos_x_canvas + 1/zoom * (-(pos_x_mouse - (e.pageX - offset.left) + $("#brainstorming-conteneur").width() / 2))));
            $('#brainstorming-canvas').css('margin-top', (pos_y_canvas + 1/zoom * (-(pos_y_mouse - (e.pageY - offset.top) + $("#brainstorming-conteneur").height() / 2))));
        }
    });
    //Quand l'utilisateur relâche la souris
    $('#brainstorming-canvas').mouseup(function() {
        grab_brain = false;
        $('#brainstorming-canvas').css('cursor', '-webkit-grab');
    });
    //MOBILE -----------------------------------------------------------------
    //Quand l'utilisateur commence d'appuyer
    var brainstorming_canvas = document.getElementById('brainstorming-canvas');
    brainstorming_canvas.addEventListener('touchstart', function(e){
        console.log('touch_start');
        pos_x_canvas = parseInt($('#brainstorming-canvas').css('margin-left'));
        pos_y_canvas = parseInt($('#brainstorming-canvas').css('margin-top'));
        pos_x_mouse = (e.changedTouches[0].pageX - offset.left) - $("#brainstorming-conteneur").width() / 2;
        pos_y_mouse = (e.changedTouches[0].pageY - offset.top) - $("#brainstorming-conteneur").height() / 2;
    }, false);
    //Quand l'utilisateur déplace son doigt
    brainstorming_canvas.addEventListener('touchmove', function(e){
        console.log('start_move');
        $('#brainstorming-canvas').css('margin-left', (pos_x_canvas + 1/zoom * (-(pos_x_mouse - (e.changedTouches[0].pageX - offset.left) + $("#brainstorming-conteneur").width() / 2))));
        $('#brainstorming-canvas').css('margin-top', (pos_y_canvas + 1/zoom * (-(pos_y_mouse - (e.changedTouches[0].pageY - offset.top) + $("#brainstorming-conteneur").height() / 2))));
    }, false);
});

// Déplacer un message de la liste d'attente jusque sur le brainstorming -----------------------------------------------------------------------------------
socket.on('add_to_board', function(data) {
    console.log(data);
    for(i in data) {
        var html = '<div class="draggable-message no-canvas-drag message-on-board selectable-message" style="';
        if(data[i].on_board) {
            //Le message a déjà été mis auparavant sur le plateau, sa position est connue
            html += 'margin-left: ' + data[i].x + '; margin-top: '+ data[i].y + ';';
        }else{
            //C'est la première fois que le message est ajouté sur le plateau --> on le met au centre
            html += 'margin-left: 0px; margin-top: 0px; ';
        }
        html += 'transform: scale(' + data[i].scale + ') rotate(' + data[i].rotation + 'deg);"';
        html += 'id_msg="' + data[i].id + '">';
        html += '<div style="padding: 0px 30px"><textarea rows="1" data-autoresize class="vertical-center message-textarea textarea-autoresize no-selectable-message" disabled style="color: ' + data[i].color  + ';cursor: default;">' + data[i].contenu + '</textarea></div></div>';
        $('#canvas-messages').append(html);
        calculate_textarea_size();
    }
});
