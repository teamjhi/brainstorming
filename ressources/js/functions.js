/****************************************[ functions.js ]********************************************/
/* Fichier Frontend */
/*          Ce fichier contient plusieurs fonctions qui peuvent être utiles dans plusieurs
*           pages du site en Frontend
*/

//Fonction qui redonne une couleur un peu plus sombre de celle qui lui est donnée en argument
function shadeColor(color, percent) {
    var R = parseInt(color.substring(1,3),16);
    var G = parseInt(color.substring(3,5),16);
    var B = parseInt(color.substring(5,7),16);

    R = parseInt(R * (100 + percent) / 100);
    G = parseInt(G * (100 + percent) / 100);
    B = parseInt(B * (100 + percent) / 100);

    R = (R<255)?R:255;
    G = (G<255)?G:255;
    B = (B<255)?B:255;

    var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
    var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
    var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));

    return "#"+RR+GG+BB;
}

//Fonction qui renvoie la valeure css "transform: rotate(deg)" d'un objet
function getRotationDegrees(obj) {
    var matrix = obj.css("-webkit-transform") ||
    obj.css("-moz-transform")    ||
    obj.css("-ms-transform")     ||
    obj.css("-o-transform")      ||
    obj.css("transform");
    if(matrix !== 'none') {
        var values = matrix.split('(')[1].split(')')[0].split(',');
        var a = values[0];
        var b = values[1];
        var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
    } else { var angle = 0; }
    return (angle < 0) ? angle + 360 : angle;
}

//Fonction qui renvoie la valeure css "transform: scale(x)" d'un objet
function getScaleValue(obj) {
    var matrix = obj.css("-webkit-transform") ||
    obj.css("-moz-transform")    ||
    obj.css("-ms-transform")     ||
    obj.css("-o-transform")      ||
    obj.css("transform");
    if(matrix !== 'none') {
        var values = matrix.split('(')[1].split(')')[0].split(',');
        var a = values[0];
        var b = values[1];
        var scale = Math.sqrt(a*a + b*b);
    } else { var scale = 1; }
    return scale;
}

//Fonction qui donne à tous les textareas une taille optimale pour que tout le texte soit visible
function calculate_textarea_size() {
    //seulement aux textareas comportant le tag data-autoresize
    $.each($('textarea[data-autoresize]'), function() {
        $(this).attr('spellcheck',false);
        $(this).attr('cols', Math.min($(this).val().split('\n')[0].length + 2, 30));
        var offset = this.offsetHeight - this.clientHeight;
        $(this).css('height', 'auto').css('height', this.scrollHeight + offset);
    });
}
