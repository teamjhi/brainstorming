/****************************************[ backups.js ]********************************************/
/* Fichier Backend */
/*      Ce fichier contient 2 fonctions qui s'occupent de créer les fichiers backup et de
*       les restaurer lors du redémarrage du serveur.
*       L'appel de ces fonctions se fait depuis le fichier /server.js
*/

module.exports = {
    create_backup: function(brains, io) {
        //Créé un fichier backup
        var fs = require('fs');
        //Notification aux pages admin qu'un backup se créé
        io.of('admins').emit('create_backup');
        //Sauvegarder l'objet "brains" dans un fichier texte
        var csv_file = JSON.stringify(brains);
        var date = new Date();
        var name = "backup_" + date.getFullYear() + "_" + date.getMonth() + "_" + date.getDate();
        name += "_" + date.getHours() + "h" + date.getMinutes() + "m" + date.getSeconds() + "s.brainbkp";
        var path = __dirname + "/ressources/backup/" + name;
        fs.writeFile(path, csv_file, function(err) {
            if(err) {
                console.log(err);
                console.log('Couldn\'t creat backup!')
            }
        });

        //Supprimer les anciens fichiers
        var path = __dirname + '/ressources/backup';
        fs.readdir(path, function(err, files) {
            files.forEach(function(file, index) {
                var file_path = path + '/' + file
                fs.stat(file_path, function(err, stat) {
                    if (err) {
                        return console.error(err);
                    }
                    var now = new Date().getTime();
                    //Supprimer tous les fichiers plus vieux de 45min
                    var endTime = new Date(stat.ctime).getTime() + (1000 * 25);
                    if (now > endTime) {
                        fs.unlink(file_path, function (err) {
                            if (err) {
                                console.error(err);
                            }
                        });
                    }
                });
            });
        });
    },

    //Check si il y a des fichiers backups dans le dossier "/ressources/backup" et load le contenu du fichier
    //dans l'objet "brains"
    read_backup: function(brains, Brain, Message, Contributeur) {
        var fs = require('fs');
        var path = __dirname + '/ressources/backup';
        var files_tab = {};
        var latest_file;
        fs.readdir(path, function(err, files) {
            //Trouver le fichier le plus récent
            files.forEach(function(file, i) {
                fs.stat(path + '/' + file, function(err, stat) {
                    if (err) {
                        return console.error(err);
                    }
                    files_tab[(stat.ctime).getTime()] = file;
                    if(i == files.length-1) {
                        latest_file = files_tab[Math.max.apply(null, Object.keys(files_tab))];
                        //Lire le fichier
                        fs.readFile(path + '/' + latest_file, {encoding: 'utf-8'}, function(err,data){
                            if (!err) {
                                try{
                                    //Transférer le fichier dans l'objet brains
                                    brains_temp = JSON.parse(data);
                                    //Passer à travers l'objet brains_temp pour y recréer les objets issus des classes (Brain, Message, Contributeur)
                                    Object.keys(brains_temp).forEach(function(k) {
                                        //Recréé le brain et y place toutes les données
                                        brains[k] = new Brain(brains, brains_temp[k].id_socket_admin, brains_temp[k].token, brains_temp[k].id, brains_temp[k].title);
                                        //foreach des messages dans le brainstorming
                                        Object.keys(brains_temp[k].messages).forEach(function(l) {
                                            var message_temp = brains_temp[k].messages[l];
                                            brains[k].messages[message_temp.id] = new Message(message_temp.contenu, message_temp.id_contributeur, message_temp.id, message_temp.on_board, message_temp.x, message_temp.y, message_temp.scale, message_temp.rotation, message_temp.color);
                                        });
                                        //foreach des contributeurs dans le brainstorming
                                        Object.keys(brains_temp[k].contributeurs).forEach(function(m) {
                                            var contributeur_temp = brains_temp[k].contributeurs[m];
                                            brains[k].contributeurs[contributeur_temp.id] = new Contributeur(contributeur_temp.socket_id, contributeur_temp.token_brain_connected, contributeur_temp.color, contributeur_temp.active, contributeur_temp.id);
                                        });
                                    });
                                    console.log('Data restored from file ' + latest_file);
                                    console.log('Currently ' + Object.keys(brains).length + ' brains working');
                                }catch (e){
                                    console.log(e);
                                    console.log('Error : data couldn\'t be restored! Starting from scratch');
                                }
                            } else {
                                console.log(err);
                                console.log('No file : data couldn\'t be restored! Starting from scratch');
                            }
                        });
                    }
                });
            });
        });
    }
}
